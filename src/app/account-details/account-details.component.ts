import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-account-details',
  templateUrl: './account-details.component.html',
  styleUrls: ['./account-details.component.css']
})
export class AccountDetailsComponent implements OnInit {

  account: any = {};

  constructor(private http: ApiService, private route: ActivatedRoute, private location: Location) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.http.get("http://localhost:8080/accounts/account/" + params["id"]).subscribe(account => {
        this.account = account;
      });
    });
  }

  saveAccount() {
    this.http.put("http://localhost:8080/accounts/account/" + this.account.id, this.account).subscribe(account => {
      alert("Cont salvat cu succes!");
    });
  }

  cancelAccount() {
    this.location.back();
  }

  deleteAccount() {
    this.http.delete("http://localhost:8080/accounts/account/" + this.account.id).subscribe(account => {
      console.log(account);
      alert("Cont sters cu succes!");
      this.location.back();
    });
  }
}
