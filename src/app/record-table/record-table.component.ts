import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';

@Component({
  selector: 'app-record-table',
  templateUrl: './record-table.component.html',
  styleUrls: ['./record-table.component.css']
})
export class RecordTableComponent implements OnInit, OnChanges {

  @Input() columns: string [];
  @Input() data: any [];
  @Output() open: EventEmitter<any> = new EventEmitter();
  
  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(test) {
    console.log(test);
  }

  onRecordClicked(event, itemId) {
    this.open.emit(itemId);
  }
}
