import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';
import { Observable } from 'rxjs';
import {Router} from '@angular/router';


@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.css']
})
export class AccountsComponent implements OnInit {

  columns: string [] = ["name", "address", "email"];
  accounts: Observable<any []>;


  constructor(private http: ApiService, private router: Router) { }

  ngOnInit() {
    this.accounts  = this.http.get("http://localhost:8080/accounts");
  }

  navigateToDetails(itemId) {
    this.router.navigate(['/Accounts/' + itemId]);
  }
}
