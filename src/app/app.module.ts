import { ApiService } from './services/api.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RecordTableComponent } from './record-table/record-table.component';
import { AccountsComponent } from './accounts/accounts.component';
import {HttpClientModule} from '@angular/common/http';
import { AccountDetailsComponent } from './account-details/account-details.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    RecordTableComponent,
    AccountsComponent,
    AccountDetailsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
